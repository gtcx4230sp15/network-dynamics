function Z = L_G__1d3pt (n, varargin)
% L_G__1d3pt: Applies matrix for spring system.
%
% L_G = L_G__1d3pt (n);  % Returns L_G explicitly
% y = L_G__1d3pt (n, x); % Returns L_G*x
%

e = ones (n, 1);
L_G = spdiags ([-e 2*e -e], -1:1, n, n);
L_G(1, 1) = 1;
L_G(n, n) = 1;

if length (varargin),
  Z = L_G * varargin{1};
else
  Z = L_G;
end

% eof

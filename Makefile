.DEFAULT_GOAL := all

CXX = g++
CXXFLAGS = -Wall -Werror
LDFLAGS = -lm

all: viral-sim$(EXEEXT)

viral-sim$(EXEEXT): viral-sim.o
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

%.o: %.cc
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -f viral-sim$(EXEEXT) *.o *~ core

# eof

#include <cassert>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <random>
#include <unordered_map>

using namespace std;

size_t
a_to_sizet (const char* s)
{
  long x_raw = atol (s); assert (x_raw > 0);
  return (size_t)x_raw;
}

double
a_to_posdouble (const char* s)
{
  double x_raw = atof (s); assert (x_raw > 0);
  return x_raw;
}

double
f (double x)
{
  return 1.0 - x;
}

double
g_x (double x)
{
  return x / (x + 1);
}

double
g (double x_i, double x_j)
{
  return g_x (x_j) - g_x (x_i);
}

void
dumpHeader (void)
{
  cout << "S,T,I,J,X" << endl;
}

void
dumpGrid (vector<double> X, const size_t N, const size_t s, const double t_s)
{
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < N; ++j) {
      cout << s << ',' << t_s << ',' << i << ',' << j << ',' << X[i + j*N] << endl;
    }
  }
}

int
main (int argc, char* argv[])
{
  if (argc < 6) {
    cerr << "usage: " << argv[0] << " <n> <x0> <t> <dt> <max_frames>" << endl;
    return 1;
  }

  const size_t N = a_to_sizet (argv[1]);
  const double X0 = a_to_posdouble (argv[2]);
  const double T_MAX = a_to_posdouble (argv[3]);
  const double DT = a_to_posdouble (argv[4]);
  const size_t F_MAX = a_to_sizet (argv[5]);
  const size_t W = ((size_t)floor (T_MAX / DT)) / F_MAX;

  cerr << "==== Parameter summary ====" << endl
       << "Creating " << N << "x" << N << " grid..." << endl
       << "Setting initial impulse of " << X0 << " at (0, 0)." << endl
       << "Simulating until t=" << T_MAX << " with dt=" << DT << '.' << endl
       << "Writing at most " << F_MAX << " frames (every " << W << " frames)" << endl
       << "==== (end parameter summary) ====" << endl
    ;
  vector<double> X (N * N);
  for (size_t i = 0; i < N; ++i)
    for (size_t j = 0; j < N; ++j)
      X[i + j*N] = 0.0;
  X[N/2 + (N/2)*N] = X0;

  dumpHeader ();
  dumpGrid (X, N, 0, 0);
  size_t saved_frames = 0;

  vector<double> X_copy (N * N);
  
  vector<double>& X_cur = X;
  vector<double>& X_next = X_copy;
  const size_t s_max = (size_t)floor (T_MAX / DT);
  for (size_t s = 0; s < s_max; ++s) {
    const double t = DT * s;
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
	const double x_ij = X_cur[i + j*N];
	double dx_ij = f (x_ij);
	const double g_ij = g_x (x_ij);
	if (i > 0) {
	  //	  if (j > 0) dx_ij += g_x (X_cur[(i-1) + (j-1)*N]) - g_ij;
	  dx_ij += g_x (X_cur[(i-1) + j*N]) - g_ij;
	  //	  if (j < (N-1)) dx_ij += g_x (X_cur[(i-1) + (j+1)*N]) - g_ij;
	}
	if (j > 0) dx_ij += g_x (X_cur[i + (j-1)*N]) - g_ij;
	if (j < (N-1)) dx_ij += g_x (X_cur[i + (j+1)*N]) - g_ij;
	if (i < (N-1)) {
	  //	  if (j > 0) dx_ij += g_x (X_cur[(i+1) + (j-1)*N]) - g_ij;
	  dx_ij += g_x (X_cur[(i+1) + j*N]) - g_ij;
	  //	  if (j < (N-1)) dx_ij += g_x (X_cur[(i+1) + (j+1)*N]) - g_ij;
	}
	X_next[i + j*N] = x_ij + (dx_ij*DT);
      } // j
    } // i
    swap (X_cur, X_next);
    if (((s % W) == 0) && (saved_frames <= F_MAX)) {
      dumpGrid (X_cur, N, s, t);
      ++saved_frames;
    }
  } // t
  
  return 0;
}

// eof

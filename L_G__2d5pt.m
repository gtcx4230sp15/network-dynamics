function Z = L_G__2d5pt (n, varargin)
% L_G__2d5pt: Graph Laplacian for a 5-point stencil on a 2-D mesh
%
% L_G = L_G__2d5pt (n);  % Returns L_G explicitly
% y = L_G__2d5pt (n, x); % Returns L_G*x
%

I = diag (ones (n, 1));
L_G__1d = L_G__1d3pt (n);
L_G__2d = kron (I, L_G__1d) + kron (L_G__1d, I);

if length (varargin),
  Z = L_G__2d * varargin{1};
else
  Z = L_G__2d;
end

% eof

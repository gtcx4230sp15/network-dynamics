# Example 18.2.3 from Newman (2004): Networks: An Introduction

source ("R-vutils.git/cmds-inc.R")
source ("R-vutils.git/util-inc.R")
library ("deSolve")

add.endpoint.text <- function (X, Y, labeler=function (x) x, ...) {
    k <- which.max (X)
    return (annotate ("text", x=X[k], y=Y[k], label=labeler (Y[k]), ...))
}

#==================================================
#Frames <- read.csv ("data--viral-sim/viral-sim-5-10-0.01-100.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-5-0.1-2-0.01-9.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-2-0.01-9.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-2-0.01-4.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-2-0.01-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-2-0.001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-100-0.1-2-0.001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-0.1-0.0001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-0.001-0.0001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-0.01-0.0001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.1-0.1-0.0001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.5-0.1-0.0001-6.csv")
#Frames <- read.csv ("data--viral-sim/viral-sim-10-0.5-1-0.0001-6.csv")

assign.if.undef ("FILEBASE", "data--viral-sim/viral-sim-10-0.5-1-0.0001-4")
assign.if.undef ("SAVE.PDF", FALSE)

#==================================================
Frames <- read.csv (sprintf ("%s.csv", FILEBASE))

#Frames.plot <- subset (Frames, S > 0)
Frames.plot <- Frames
#Q <- ggplot (Frames.plot, aes (x=I, y=J, z=X))
Q <- ggplot (Frames.plot, aes (x=as.numeric (I), y=as.numeric (J), z=X))

#Q <- Q + stat_contour (aes (colour=..level..))
#Q <- Q + stat_contour (aes (colour=..level..), breaks=seq (from=0, to=1, by=0.1))
#Q <- Q + stat_contour (aes (colour=..level..), breaks=c (0.05, 0.1, 0.2, 0.3, 0.4, 0.5))
#Q <- Q + scale_colour_gradient (low="blue", high="red", limits=c (0, 1))

if (TRUE) {
#  color.min = "#b1cc70"
#  color.max = "#53777a"
  color.max = "#cc333f"
  color.min = "#edc951"
  color.labels = "black"
} else if (PALETTE == "HPCGARAGE") {
  color.min = PAL.HPCGARAGE[["blue"]]
  color.max = PAL.HPCGARAGE[["red"]]
  color.labels = "white"
}
Q <- Q + scale_fill_gradient (name="", high=color.max, low=color.min) #, limits=c (0, 1), breaks=seq (from=1, to=0, by=-.1))
Q <- Q + geom_tile (aes (fill=X))

Q <- Q + scale_x_continuous (limits=as.numeric (range (Frames.plot$I)))
Q <- Q + scale_y_continuous (limits=as.numeric (range (Frames.plot$J)))
Q <- Q + facet_wrap (~ T)
Q <- Q + xlab ("")
Q <- Q + ylab ("")
Q <- Q + coord_equal (ratio=1)

setDevSlide ()
#print (direct.label (Q))
print (Q)
ggsave.pdf.if (Q, sprintf ("%s.pdf", FILEBASE), SAVE.PDF)

# eof
